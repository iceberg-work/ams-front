import { createApp } from 'vue'
// @ts-ignore
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import App from './App.vue'
import * as VueRouter from 'vue-router'
import routes from './config/route'
import axios from 'axios'

// 全局设置 axios 发送请求带上cookie
//https://blog.csdn.net/weixin_42425970/article/details/104082540
axios.defaults.withCredentials = true;

const router = VueRouter.createRouter({
    history: VueRouter.createWebHashHistory(),
    routes,
});


// 全局监听路由
router.beforeEach((to, from, next) => {
    next();
});

const app = createApp(App);
app.use(router);
app.use(ElementPlus);
app.mount('#app');