// 定义一些路由
import Index from "../pages/Index.vue";
import User from "../pages/User.vue";
import Role from "../pages/Role.vue";
import Attendance from "../pages/Attendance.vue";
import Permission from "../pages/Permission.vue";
import Leave from "../pages/Leave.vue";
import Task from "../pages/Task.vue";
import Departmental_subsidy from "../pages/Departmental_subsidy.vue";
import Rank_salary from "../pages/Rank_salary.vue";
import Department from "../pages/Department.vue";
import Rank from "../pages/Rank.vue";
import Personnel from "../pages/Personnel.vue";
import Login from "../pages/Login.vue";
import Content from "../Content.vue";
import Absence from "../pages/Absence.vue";
import SignIn from "../pages/SignIn.vue";
import Salary from "../pages/Salary.vue";
import Task_type from "../pages/Task_type.vue";

const routes = [
    { path: '/', component: Index},
    { path: '/role', component: Role},
    { path: '/user', component: User},
    { path: '/attendance', component: Attendance},
    { path: '/permission', component: Permission},
    { path: '/leave', component: Leave},
    { path: '/task', component: Task},
    { path: '/departmental_subsidy', component: Departmental_subsidy},
    { path: '/rank_salary', component: Rank_salary},
    { path: '/department', component: Department},
    { path: '/rank', component: Rank},
    { path: '/personnel', component: Personnel},
    { path: '/index', component: Index},
    { path: '/login', component: Login},
    { path: '/content', component: Content},
    { path: '/absence', component: Absence},
    { path: '/signIn', component: SignIn},
    { path: '/salary', component: Salary},
    { path: '/task_type', component: Task_type},
]

export default routes;